Jack Cousteau
==============

``cousteau`` is a wrapper around ``jackd``.

It starts jackd and patches ~/.asoundrc to enable a JACK->ALSA bridge.

When jackd either dies (or is killed by another process) or cousteau is interrupted by ctrl-c or some other process, it restores ALSA to normal or reports an error if unable to do so.

This is especially useful for Slackware.


Setup
------

You need two things before this script can be of use to you:

1. An ALSA->JACK bridge. There are instructions on how to create this on the JACK website http://jackaudio.org/faq/routing_alsa.html

2. A diff (patch file) of an ALSA ~/.asoundrc file and a JACK->ALSA ~/.asoundrc file (call it ~/.asoundrc-switch), which cousteau will use to patch and restore your ALSA->JACK bridge.

Full details on how to do all of this is forthcoming on http://slackermedia.info, but as of 16-06-2015, the docs are still being written. Have patience! 

